# TelegramWindoofBot
A Telegram bot that corrects you. An Example may be running here [@windoofbot](https://t.me/windoofbot).

This Project is moved to [TgTriggerBots](https://gitlab.com/BergiuTelegram/TgTriggerBots).

## Dependencies
```
sudo apt install openjdk-8-jdk
```

## Installation
```shell
git clone https://gitlab.com/BergiuTelegram/TgWindoofBot && cd TgWindoofBot
./config.sh
./build
```

## Configuration
- Create a new TelegramBot with the [Botfather](https://telegram.me/botfather)
- Disable privacy settings for the bot
- Write your Botname into the file `BOTNAME` and your Token into the file `TOKEN`

## Run
- `./run`
